var sql = require('mssql');

var config = {
    server: '192.168.56.1',
    database: 'Chinook',
    user: 'sa',
    password: 'e',
    port: 1433
};


// var config = {
//     server : 'thinema.iptime.org',
//     database : 'Chinook',
//     user : 'ChinookOwner',
//     password : '1',
//     port : 3423
// };

// sql.connect(config, err => {

//     var lower = 1;
//     var upper = 2;

//     var request = new sql.Request();
//     request.stream = true;

//     var query = 'select top 3 * from Track where UnitPrice between ';
//     query += lower;
//     query += ' and ';
//     query += upper;

//     request.query(query);

//     request.query('select top 3 * from Artist');

//     request.on('row', row => {
//         console.log(`Name of ${row.Name} is ${row.unitPrice}`);
//     })
// });

//async / await
var pool1 = new sql.ConnectionPool(config, err => {
    var request = pool1.request();

    request.input('Lower', sql.Numeric, 1);
    request.input('Upper', sql.Numeric, 2);

    request.execute(
        'Track_SelectByUnitPrice',
        (err, result, returnValue) => {
            console.log(result.rowsAffected);
            result.recordset.forEach(row => {
                console.log(`Name of ${row.Name} is ${row.UnitPrice}`);
            }, this);
        });
});


    // sql.connect(config, err => {
    //     var request = new sql.Request();

    //     request.input('Lower', sql.Numeric, 1);
    //     request.input('Upper', sql.Numeric, 2);

    //     request.execute(
    //         'Track_SelectByUnitPrice', 
    //         (err, result, returnValue) => {
    //             console.log(result.rowsAffected);
    //             result.recordset.forEach(row => {
    //                 console.log(`Name of ${row.Name} is ${row.UnitPrice}`);
    //             }, this);
    //     });
// });
